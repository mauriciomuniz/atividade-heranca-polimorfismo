import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class Main {

    public static void main(String[] args) throws Exception {

        Dados dados = new Dados();
        IO io = new IO();
        List<Pessoa> dadosArmazenados = new ArrayList<>();

        int numeroPessoas = io.iniciaProcesso();

        dadosArmazenados = dados.armazena(numeroPessoas);

        int acaoSolicitada = io.verificaContinuidade();

        String email = io.direcionaProcesso(acaoSolicitada);


        if (acaoSolicitada == 2) {
            dados.deleta(dadosArmazenados, email);
        } else if (acaoSolicitada == 3) {
            dados.consulta(dadosArmazenados, email);
        }
    }
}
