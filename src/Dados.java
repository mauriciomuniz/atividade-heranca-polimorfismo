import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Dados {

    public List<Pessoa> armazena(int numeroPessoas) {
        List<Pessoa> pessoas = new ArrayList<>();

        for (int i = 0; i < numeroPessoas; i++) {
            Pessoa pessoa = new Pessoa();
            Scanner scanner = new Scanner(System.in);

            System.out.println("Qual o seu nome?");
            String nomePessoa = scanner.nextLine();

            System.out.println("Qual o seu e-mail?");
            String emailPessoa = scanner.nextLine();

            pessoa.setEmail(emailPessoa);
            pessoa.setNome(nomePessoa);
            pessoas.add(pessoa);
        }

        return pessoas;
    }

    public String deleta(List<Pessoa> pessoas, String email) {
        int i = 0;
        List<Pessoa> removerPessoa = new ArrayList<>();
        for (Pessoa pessoa : pessoas) {
            if (pessoa.getEmail().equals(email)) {
                removerPessoa.add(pessoa);
            }
            i++;
        }
        pessoas.remove(removerPessoa);
        System.out.println("email deletado: " + email);
        return email;
    }

    public void consulta(List<Pessoa> pessoas, String email) {

        for (Pessoa pessoa : pessoas) {
            if (pessoa.getEmail().equals(email)) {
                System.out.println("Nome consultado: " + pessoa.getNome() + " e seu E-mail: " + pessoa.getEmail());
            }

        }
    }
}
