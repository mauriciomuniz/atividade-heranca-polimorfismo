import java.util.HashMap;
import java.util.Scanner;

public class IO {

    public int iniciaProcesso() {

        Scanner scanner = new Scanner(System.in);

        System.out.println("Quantas pessoas serão adicionadas a lista?");
        int numeroPessoas = scanner.nextInt();

        return numeroPessoas;
    }

    public int verificaContinuidade() {
        Scanner scanner = new Scanner(System.in);

        System.out.println("Digite 2 para deletar dados, 3 para pesquisar dados ou qualquer outro número para terminar o processamento:");
        int acaoSolicitada = scanner.nextInt();

        return acaoSolicitada;
    }

    public String direcionaProcesso(int acaoSolicitada) throws Exception{
        Scanner scanner = new Scanner(System.in);

        if (acaoSolicitada != 2 && acaoSolicitada != 3) {
            String msg = "Fim do processamento";
            throw new Exception(msg);

        } else {
            System.out.println("Qual o e-mail do contato");
            String email = scanner.nextLine();

            return email;
        }
    }
}
